<?php

// dovada faptului ca o functie se poate apela inaintea declaratiei ei
echo dinxiny("tantar");

// declararea functiei
function dinxiny($variabila){
	$result = "";
	if ($variabila == "tantar"){
		$result = "armasar";
	}
	return $result."<br />";
}

echo dinxiny("tantar");

function prinvaloare($valoare){
	echo $valoare;
}

$salam = "Florin";
prinvaloare($salam);

function prinreferinta(&$referinta){
	$referinta .= " tata banilor";
}

$mocanu = "Dani";

function facultativ($a,$b,$c=0,$d=0){
	return $a+$b+$c+$d;
}

echo facultativ(1,2,3,4);
echo facultativ(1,2,3);
echo facultativ(1,2);

echo "<br />";

function functiecuparamfacultativ($param = "parametru"){
	echo $param."<br />";
}

functiecuparamfacultativ();
functiecuparamfacultativ(NULL);
functiecuparamfacultativ("altceva");

function suma(...$lista){
var_dump($lista);
}

suma(1,2,3);

function suma($a){

}

suma(array(1,2,3))