<?php

function f1($a,$b,$c=1){
	$result = ($a+$b)**$c;
	return $result;
}

echo f1(1,2,3);

echo "<br />";
function f2($salar,$flag=false){
	$rest = intval($salar*0.45);
	if(!$flag){
		$rest += intval($salar*0.2);
	}
	echo "Taxe: $rest";
	if($flag){
		echo " (IT)";
	}
	echo "<br />";
}

f2(4500);
f2(4500,true);
f2(6200,true);

function afisare($rezultat){
	echo $rezultat."<br />";
}

afisare("da");

afisare("nu");

function f5($nume,$fructe){
	foreach ($fructe as $fruct){
		echo $nume." are ".$fruct."<br/>";
	}	
}

f5("calin",array("mere","pere"));