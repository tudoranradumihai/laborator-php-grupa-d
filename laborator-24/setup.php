<?php

$connection = mysqli_connect("localhost","root","","DB");

if($connection){
	$query = "CREATE TABLE transactions (
		id INT(11) PRIMARY KEY AUTO_INCREMENT,
		source VARCHAR(3),
		target VARCHAR(3),
		amount FLOAT(11,2),
		rate FLOAT(11,4),
		total FLOAT(11,2),
		createddate DATETIME DEFAULT CURRENT_TIMESTAMP
	);";

	$result = mysqli_query($connection,$query);
	if($result){
		echo "Table 'transactions' has been succesfully created.";
	}
	mysqli_close($connection);
}