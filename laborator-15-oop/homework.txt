Se da clasa C1 ce contine 2 proprietati publice p1 si p2.
Clasa C1 mai contine si proprietatea privata p3. 
Se creeaza m1 si m2, metode de setat si returnat valoarea proprietatii p3.

Clasa C1 se afla in folderul "Untold"

Se da clasa C2 (in folderul Neversea) cu 2 proprietati private p1 si p2.
Se creaza metode de setat si returnat pentru ambele proprietati.

Se da clasa C3 (in folderul ClujArena din folderul Untold) identica cu clasa C2.

in fisierul index.php se vor initializa toate cele 3 obiecte si se vor atribui valorui tuturor proprietatilor.

O seara placuta!
