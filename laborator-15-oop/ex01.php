<?php

Class NUME_CLASA {
	public $nume_proprietate;
}

$nume_obiect = new NUME_CLASA();

// http://php.net/manual/en/oop5.intro.php
// http://php.net/manual/en/language.oop5.basic.php
// http://php.net/manual/en/language.oop5.properties.php

Class Users {

	public $firstname;
	public $lastname;

	public function show(){
		echo "Hello!";
	}

	public function showFullName(){
		echo $this->firstname." ".$this->lastname;
	}

}

$user = new Users();
$user->firstname = "Radu";
$user->lastname = "Tudoran";
//var_dump($user);
$user->show();
echo "<br />";
$user->showFullName();


Class Product {
	public $name;
	public $description;
	public $price;
	public $stock;

	public function totalStockPrice(){
		return $this->price*$this->stock;
	}

	public function displayTotalStockPrice(){
		echo "Pretul stockului total al produsului $this->name este: ".self::totalStockPrice();
	}
}


$product = new Product();
$product->name = "Televizor cu mileu";
$product->price = 99;
$product->stock = 5;
$product->displayTotalStockPrice();