<?php

// 1. se deschide fisierul cu optiune de scriere si se introduce in variabila resursa
$file = fopen("file.txt","w");

// 2.0. se declara continutul
$line = "CONTENT";

// 2.1. se adauga continutul in variabila de tip resursa
fputs($file,$line);

// 3. se inchide variabila resursa
fclose($file);

// -----
Class FileSystem {
	private $file;
	public function __construct(){
		$this->file = fopen("file.txt","w");
	}
	public function write($content){
		fputs($this->file,$content.PHP_EOL);
	}
	public function read(){
		return fgets($this->file);
	}
	public function __destruct(){
		fclose($this->file);
	}
}
// 1. se deschide fisierul cu optiune de scriere si se introduce in variabila resursa
// $file = fopen("file.txt","w"); DEVINE
$object = new FileSystem();
// 2.0. se declara continutul
$line = "CONTENT";
// 2.1. se adauga continutul in variabila de tip resursa
// fputs($file,$line); DEVINE
$object->write($line);
// 3. se inchide variabila resursa
unset($object); // FACULTATIV
// -----
Class FileSystem {
	private $file;
	public function __construct($filename,$mode){
		$this->file = fopen($filename,$mode);
	}
	public function write($content){
		fputs($this->file,$content.PHP_EOL);
	}
	public function readLine(){
		return fgets($this->file);
	}
	public function readFile(){
		$file = "";
		while($line = fgets($this->file)){
			$file .= $line.PHP_EOL;
		}
		return $file;
	}
	public function __destruct(){
		fclose($this->file);
	}
}

$object2 = new FileSystem("file.txt","r");
echo $object2->readLine();