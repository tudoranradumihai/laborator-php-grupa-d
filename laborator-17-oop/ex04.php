<?php

Class Database {

	private $connection;

	const HOSTNAME = "localhost";
	const USERNAME = "root";
	const PASSWORD = "";
	const DATABASE = "caca";

	public function __construct(){
		$this->connection = new mysqli(self::HOSTNAME,self::USERNAME,self::PASSWORD,self::DATABASE);
		// Check connection
		if ($this->connection->connect_error) {
			die("Connection failed: " . $this->connection->connect_error);
		}
	}

	public function query($query){
		return $this->connection->query($query);
	}

	public function __destruct(){
		$this->connection->close();
	}

}

$db = new Database();