<?php

Class FileSystem {
	protected $file;

	public function __construct($filename,$mode){
		$this->file = fopen($filename,$mode);
	}

	public function __destruct(){
		fclose($this->file);
	}
}

Class Write extends FileSystem {

	public function __construct($filename){
		parent::__construct($filename,"w");
	}

	public function write($content){
		fputs($this->file,$content.PHP_EOL);
	}
}

$obj = new Write("file.txt");

Class Model {

	protected $id;

	public function setId($id){
		$this->id = $id;
	}

	public function getId(){
		return $this->id;
	}

}

Class Users extends Model {

	protected $firstname;
	/* ... */
	protected $status;

	public function __construct($id, $firstname, $lastname, $email, $password, $status){
		$this->id = $id;
		$this->firstname = $firstname;
		/* ... */
		$this->status = $status;
	}

	public function setFirstname($firstname){
		$this->firstname = $firstname;
	}

	public function getFirstname(){
		return $this->firstname;
	}
	/* ... */

}

Class Administrator extends Users {

	public function __construct($id,$firstname,$lastname,$email,$password){
		parent::__construct($id,$firstname,$lastname,$email,$password,"ADMIN");
	}

}