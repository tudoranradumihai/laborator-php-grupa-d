<?php

Class Database {

	private $connection;

	const HOSTNAME = "localhost";
	const USERNAME = "root";
	const PASSWORD = "";
	const DATABASE = "";

	public function __construct(){
		$this->connection = mysqli_connect(self::HOSTNAME,self::USERNAME,self::PASSWORD,self::DATABASE);
	}

	public function execute($query){
		return mysqli_query($this->connection,$query);
	}

	public function __destruct(){
		mysqli_close($this->connection);
	}

}