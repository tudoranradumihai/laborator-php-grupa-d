<?php
// se verifica daca se vine prin HTTP POST
// numele fisierul $_POST["file"]
// continutul $_POST["content"]
// se deschide un fisier cu numele din variabila post si extensia .txt
// se va introduce continutul in interiorul fisierului
// se va inchide fisierul

// daca se intra pe fisier fara sa vina prin post,  se va face un redirect catre new.php 
// daca se va face cu succes operatiunea se va face un redirect catre index.php
// ET 10 min

if ($_SERVER["REQUEST_METHOD"]=="POST"){
	$filename = "files/".$_POST["file"].".txt";
	$content = $_POST["content"];
	$file = fopen($filename,"w");
	fputs($file,$content);
	fclose($file);
	header("Location: index.php");
} else {
	header("Location: new.php");
}