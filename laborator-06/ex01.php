<?php
$string = "lorem ipsum dolor";

echo $string."<br />";
echo ucfirst($string)."<br />";
echo ucwords($string)."<br />";
echo strtoupper($string)."<br />";
echo strtolower("LOREM ipsum Dolor")."<br />";

$text = "ana are mere";
echo str_replace("mere", "pere", $text)."<br />";

$text = "ana si laura au mere";
echo str_replace(array("ana","laura"), "fata", $text)."<br />";

$text = "mamemimomu";
echo str_replace(array("a","e","i","o","u"),"vocala",$text)."<br />";

$text = "  lorem ipsum dolor   ";
var_dump($text);
var_dump(trim($text));

$text = "Abecedar";
echo substr($text,3,2)."<br />";
echo substr($text,3)."<br />";
$text = "Popescu";
echo substr($text,-4)."<br />";

$text = "ana are mere";
echo substr_count($text,"a")."<br />";

var_dump(strpos($text,"a"));

var_dump(strrpos($text,"a"));

var_dump(strlen($text));

$text= "CazFoarteRar";
echo lcfirst($text)."<br/>";	