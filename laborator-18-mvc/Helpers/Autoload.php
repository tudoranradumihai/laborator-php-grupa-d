<?php

function __autoload($class){
	$folders = array(
		"Controllers",
		"Models"
	);
	foreach ($folders as $folder){
		$filepath = $folder."/".$class.".php";
		if(file_exists($filepath)){
			include $filepath;
		}
	}
}