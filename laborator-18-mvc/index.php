<?php
/*
MVC
- index.php este fisierul de baza in cadrul structurii MVC.
- structura cuprinde folder-ele Controllers, Models si Views
- index.php va identifica din parametrii $_GET ce controller si actiune va fi apelata
DESPRE MVC
- pentru fiecare tabel din baza de date se va face o clasa model 
- pentru fiecare model se va face o clasa controller
- pentru fiecare controller se va face un folder in Views
- pentru fiecare actiune din controller se va face un fisier in Views/
***
Front Controller
- de citit (!)
*/

/*
LINK

index.php?C=Users&A=list

NOTATIE $_GET
"C"
- se refera la ce controller va fi utilizat
- $_GET["C"] = "Users" -> se va utiliza UsersController (clasele incep cu litera mare)
"A"
- se noteaza "A" de la action si se refera la ce metoda va fi apelata
- $_GET["A"] = "list" -> se va utiliza listAction (metodele incep cu litera mica si toate metodele din clasele controller se termina cu cuvantul "Action")
*/

include "Helpers/Autoload.php";
// Se verifica daca exista cheia C in $_GET
if(array_key_exists("C", $_GET)){
	// Daca exista se creaza o variabila $controller cu numele clasei controller ce va fi initializata
	$controller = $_GET["C"]."Controller";
} else {
	// Daca nu exista, se defineste numele clasei Default
	$controller = "DefaultController";
}

if(array_key_exists("A", $_GET)){	
	$action = $_GET["A"]."Action";
} else {
	$action = "defaultAction";
}

// Se verifica daca exista clasa cu numele construit in variabila $controller
if(class_exists($controller)){
	// Se initializeaza un obiect din clasa $controller
	$object = new $controller();
	// Se verifica daca exista metoda $action in cadrul obiectului initializat din clasa $controller
	if(method_exists($object, $action)){
		// Se apeleaza metoda $action din clasa $controller
		$object->$action();
	} else {
		echo "ERROR: Method '$controller::$action' does not exists.";
	}
} else {
	echo "ERROR: Class '$controller' does not exists.";
}

/*
LEARN:
http://php.net/manual/en/language.oop5.php
de invatat pana la inclusiv - http://php.net/manual/en/language.oop5.abstract.php
READ:
https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller
HOMEWORK:
- de facut deasupra un meniu care are 3 linkuri
1. homepage
2. Default/aboutUs
3. Default/contact

in aboutus puneti un text cu 3 paragrafe
 
in contact puneti un formular cu nume,prenume,subiect si mesaj + submit

*/