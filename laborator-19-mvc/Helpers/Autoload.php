<?php

// http://php.net/manual/en/language.oop5.autoload.php
function __autoload($class){
	$folders = array(
		"Controllers",
		"Helpers",
		"Models"
	);
	foreach ($folders as $folder){
		$filepath = "$folder/$class.php";
		// http://php.net/manual/en/function.file-exists.php
		if (file_exists($filepath)){
			include $filepath;
		}
	}
}