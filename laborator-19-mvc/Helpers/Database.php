<?php

Class Database {

	private $connection;

	public function __construct(){
		$this->connection = new mysqli("localhost","root","","laborator-19-mvc");
	}

	public function query($query){
		return $this->connection->query($query);
	}

	public function __destruct(){
		$this->connection->close();
	}

}