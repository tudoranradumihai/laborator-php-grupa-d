<?php

function initialize(){
	// http://php.net/manual/en/function.array-key-exists.php
	if (array_key_exists("C", $_GET)){
		$controller = ucfirst($_GET["C"])."Controller";
	} else {
		$controller = "DefaultController";
	}
	if (array_key_exists("A", $_GET)){
		$action = lcfirst($_GET["A"])."Action";
	} else {
		$action = "defaultAction";
	}
	// http://php.net/manual/en/function.class-exists.php
	if (class_exists($controller)){
		$object = new $controller();
		// http://php.net/manual/en/function.method-exists.php
		if (method_exists($object, $action)){
			$object->$action();
		} else {
			echo "ERROR: '$controller::$action' doesn't exists.";
		}
	} else {
		echo "ERROR: '$controller' doesn't exists.";
	}
}