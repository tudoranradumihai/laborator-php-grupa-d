<?php

Class Products {

	public $id;
	public $name;
	public $manufacturer;
	public $description;
	public $price;
	public $stock;

	public function __construct($id,$name,$manufacturer,$description,$price,$stock){
		$this->id 			= $id;
		$this->name 		= $name;
		$this->manufacturer = $manufacturer;
		$this->description 	= $description;
		$this->price 		= $price;
		$this->stock 		= $stock;
	}

	public static function findAll(){
		$query = "SELECT * FROM products";
		$database = new Database();
		$result = $database->query($query);

		$array = array();
		while ($row = $result->fetch_assoc()){
			$object = new Products($row["id"],$row["name"],$row["manufacturer"],$row["description"],$row["price"],$row["stock"]);
			array_push($array,$object);
		}
		return $array;
	}

}