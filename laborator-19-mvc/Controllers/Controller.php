<?php

// http://php.net/manual/en/language.oop5.abstract.php
Abstract Class Controller {

	// by creating an abstract public function, all the classes that will extend the abstract class controller need to have a method called defaultAction().
	abstract public function defaultAction();

}