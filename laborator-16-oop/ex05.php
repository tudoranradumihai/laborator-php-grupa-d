<?php

Class A {

	public $p1;

}

Class B extends A {

	public $p2;

}

Class C extends B {

	public $p3;

}

$object = new A();
var_dump($object);

$object2 = new B();
var_dump($object2);

$object3 = new C();
var_dump($object3);