<?php

Class C1 {
	public $p1;
	private $p2;
	protected $p3;
	protected $p4;
	public function setP2($p){
		$this->p2 = $p;
	}
	public function getP2(){
		return $this->p2;
	}
	public function setP3($p){
		$this->p3 = $p;
	}
	public function getP3(){
		return $this->p3;
	}
	public function setP4($p){
		$this->p4 = $p;
	}
	public function getP4(){
		return $this->p4;
	}
}

Class C2 extends C1 {
	private $p5;
	public function __construct($x,$y){
		$this->p1 = $x;
		$this->p5 = $y;
	}
	public function setP5($p){
		$this->p5 = $p;
	}
	public function getP5(){
		return $this->p5;
	}
}

$obj2 = new C2(1,2);


?>

Se cere clasa C1 care contine proprietatea publica p1, proprietatea privata p2 si proprietatea p3 si p4 protected
se cere crearea de metode de setare si returnare a proprietatilor p2,p3 si p4

Se cere clasa C2 ce extinde lasa C1 si care la instantierea obiectului va seta automat prorietatea p1 (din clasa c1) si propietatea privata p5 din clasa C2
Se cer si metode get/set pentru p5