<?php

Class C1 {
	public $p1; // merge in C1 si in exterior
	protected $p2; // merge in C1, nu merge in exterior, dar merge in toti copiii lui C1
	private $p3; // merge in C1

	public function m1(){
		$this->p1;
		$this->p2;
		$this->p3;
	}
}

Class C2 extends C1 {
	public $p4; // merge in C2 si in exterior

	private $p6; // merge in C2

	public function m1(){
		$this->p1; // DA
		$this->p2; // DA
		$this->p3; // NU
	}
}
