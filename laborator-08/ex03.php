<?php

function generateWord(){
	$cuvant = array();
	$vocale = array("a","e","i","o","u");
	$consoane = array("b","c","d","f","g","h","j","k","l","m","n","p","q","r","s","t","v","w","x","y","z");
	for ($i=0;$i<rand(2,10);$i++){
		if ($i%2==0){
			array_push($cuvant, $consoane[rand(0,count($consoane)-1)]);
		} else {
			array_push($cuvant, $vocale[rand(0,count($vocale)-1)]);
		}
	}
	return implode("",$cuvant);
}

function generateSentence(){
	$propozitie = array();
	for($i=0;$i<rand(3,15);$i++){
		array_push($propozitie, generateWord());
	}
	return implode(" ",$propozitie).".";
}

function generateParagraph(){
	$paragraph = array();
	for($i=0;$i<rand(5,20);$i++){
		array_push($paragraph, ucfirst(generateSentence()));
	}
	return implode(" ",$paragraph).".";
}