<?php
/*

$keys = array("firstname","lastname","cnp","address","city");

Se cere crearea unui array $user de 5 elemente cu cheile valorilor array-ului $keys. Se cere completarea valorilor aferente fiecarei chei dupa cum urmeaza. Toate valorile exceptand cea aferenta cheii "cnp" va contine string de X caractere. Valoarea cheii "cnp" va contine un numar de 13 cifre.
*/

$keys = array("firstname","lastname","cnp","address","city");

$user = array();

foreach ($keys as $key) {
	if ($key == "cnp") {
		$user[$key] = rand(1000000000000,2999999999999);
	} else {
		$user[$key] = "string";
	}
}