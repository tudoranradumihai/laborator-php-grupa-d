<?php
include "ex03.php";

$paragraph = generateParagraph();

/*
Prima propozitie sa fie incadrata intr-un <h1>*propozitie*</h1>\
A doua si a 3 propozitie trebuie sa fie incadrate intr-un singur <p>
A patra propozitie si a 5-a o sa fie incadrate intr-un alt <p>, dar de aceasta data, propozitia 5 o sa fie subincadra in <b>
*/

$paragraph = explode(".",$paragraph);
$i=1;
$newParagraph = array();
foreach ($paragraph as $sentence){
	$sentence = trim($sentence);
	if ($i==1){
		$newParagraph[] = "<h1>".$sentence.".</h1>";
	}
	if ($i==2){
		$newParagraph[] = "<p>".$sentence.".";
	}
	if ($i==3){
		$newParagraph[] = $sentence.".</p>";
	}
	if ($i==4){
		$newParagraph[] = "<p>".$sentence.".";
	}
	if ($i==5){
		$newParagraph[] = "<b>".$sentence.".</b></p>";
	}
	$i++;
}

echo implode(" ",$newParagraph);