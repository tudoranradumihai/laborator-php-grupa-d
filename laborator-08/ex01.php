<?php
/*
Se da variabila $a = array(). Folosind un ciclu si functia rand, sa se genereze 5 elemente in interiorul acestuia in intervalul 0 - 100.
*/

$a = array(); // se declara un array, in cazul in care arrayul nu se declara, pe linia 0 se va returna o eroare pentru ca arrayul nu va exista.

// se face un ciclu de 5 pasi
for ($i=1;$i<=5;$i++) {
	$a[] = rand(0,100); // se injecteaza in $a o valoare numerica rezultata din executia functiei rand.
	// array_push($a,rand(0,100));
}

var_dump($a);