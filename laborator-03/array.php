<?php

// declarare array gol
$a1 = array();

// declarare array neasociativ cu doua valori
// V1
$a2 = array(1,2);
// V2
$a2 = array();
$a2[] = 1;
$a2[] = 2;

// declarare array asociativ
// V1
$a3 = array(0=>1,1=>2);
// V2
$a3 = array();
$a3[0] = 1;
$a3[1] = 2;

// array cu valori booleene
$a4 = array(true,false,true);
// float
$a5 = array(1.5,2.7);
// string
$a6 = array("audi","biturbo");
// mixt
$a7 = array(true,1,1.5,"adevaraaaat");

// exemplu
$user = array("Radu","Tudoran","187..");
$user = array();
$user["firstname"] = "Radu";
$user["lastname"] = "Tudoran";
$user["cenepeu"] = "187..";
echo $user["firstname"]." ".$user["lastname"]."<br />";
echo "$user[firstname] $user[lastname]"."<br />";