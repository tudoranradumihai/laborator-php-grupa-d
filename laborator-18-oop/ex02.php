<?php
Abstract Class FileSystem {
	protected $content;
	public function __construct($filename,$mode){
		$this->content = fopen($filename,$mode);
	} 
	public function __destruct(){
		fclose($this->content);
	}
}
Class Write extends FileSystem {
	public function __construct($filename){
		parent::__construct($filename,"w");
	}
	public function writeLine($line){
		fputs($this->content,$line);
	}
}
$o1 = new Write("file.txt");
$o1->writeLine("Lorem Ipsum");

$o0 = new FileSystem("file.txt","w");