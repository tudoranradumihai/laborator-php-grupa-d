<?php

Class Vehicle {
	protected $wheels;
}
Class AutoVehicle extends Vehicle {
	protected $cc;
	protected $type;
	public function __construct(){
		$this->wheels = 4;
	}
}
Class Mercedes extends AutoVehicle {
	public function __construct($cc){
		$this->cc = $cc;
		switch($cc){
			case 2.2:
				$this->type="diesel";
				break;
		}
	}
}
Class CKlasse extends Mercedes {

}

$o = new CKlasse(2.2);