<?php

Abstract Class C1 {

	abstract public function m1();
	// o metoda abstracta obliga toti copiii clasei abstracte sa detina o metoda cu numele metodei abstracte

} 

Class C2 extends C1 {

	public function m1(){
		// ...
	}

}