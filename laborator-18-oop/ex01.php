<?php

Class C1 {
	public function m1(){
	}
}
$o1 = new C1();
$o1->m1();


function f1(){

}
f1();

Class C2{
	public static function m1(){
	}
	public static function m2(){
		self::m3();
	}
	public function m3(){
	}
}
$o2 = new C2();
$o2->m1();
$o2->m2();

C2::m1();
C2::m2(); // asta nu o sa mearga pentru ca in m2 se apeleaza o metoda ne statica