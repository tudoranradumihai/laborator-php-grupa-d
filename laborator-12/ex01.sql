/*
CREATE DATABASE gdl12;
*/

DROP TABLE IF EXISTS cities;
DROP TABLE IF EXISTS countries;

CREATE TABLE countries (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL
);

INSERT INTO countries (name)
	VALUES ("Franta"),
	("Letonia"),
	("Italia"),
	("Romania"),
	("Spania"),
	("Reunion");

CREATE TABLE cities (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	population INT(11),
	latitude FLOAT(11,7),
	longitude FLOAT(11,7),
	description TEXT,
	country INT(11),
	FOREIGN KEY (country) REFERENCES countries(id)
);

INSERT INTO cities (name,country)
	VALUES ("Alba-Iulia",4),
	("Brasov",4),
	("Cluj-Napoca",4),
	("Piatra Neamt",4),
	("Galati",4),
	("Timisoara",4);



