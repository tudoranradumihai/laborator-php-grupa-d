/* select all items from table */
SELECT * FROM countries;

/* select id and name from table*/
SELECT id,name FROM countries;

/* select first item from table*/
SELECT * FROM countries LIMIT 1;

/* select item with id=1*/
SELECT * FROM countries WHERE id=1;

/* select item(s) with name equal to 'Romania'*/
SELECT * FROM countries WHERE name LIKE 'Romania';

/* select items who's name contains the letter R*/
SELECT * FROM countries WHERE name LIKE '%R%';

/* select items who's name starts with the letter R*/
SELECT * FROM countries WHERE name LIKE 'R%';

/* select items and order them alphabetically by column name*/
SELECT * FROM countries ORDER BY name ASC; /* DESC */