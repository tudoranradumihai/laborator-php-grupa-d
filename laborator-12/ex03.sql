SELECT cities.id AS 'id', cities.name AS 'city', countries.name AS 'country'
FROM cities
INNER JOIN countries ON cities.country = countries.id;