CREATE TABLE users (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	firstname VARCHAR(255),
	lastname VARCHAR(255),
	gender INT(11),
	birthdate VARCHAR(255),
	address VARCHAR(255),
	zip VARCHAR(255),
	city VARCHAR(255),
	phone VARCHAR(255),
	emailaddress VARCHAR(255) UNIQUE,
	password VARCHAR(255)
);

INSERT INTO users 
(firstname,lastname,birthdate,address,zip,city,phone,emailaddress,password)
VALUES
('','','','','','','','','');