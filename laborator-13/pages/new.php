<?php
	function showField($key){
		if(array_key_exists("values", $_SESSION)){
			if(array_key_exists($key, $_SESSION["values"])){
				echo $_SESSION["values"][$key];
			}
		}
	}
?>
<form method="POST" action="index.php?page=create">
	<div class="row">
		<h1>Create New Account</h1>
		<?php
			if(array_key_exists("errors", $_SESSION)){
				foreach ($_SESSION["errors"] as $error){
					echo "<div class=\"alert alert-danger\" role=\"alert\">$error</div>";
				}
				unset($_SESSION["errors"]);
			}
		?>
		<div class="col-md-6">
			<h2>1. Personal Information</h2>
			<div class="form-group">
				<label for="firstname">Firstname</label>
				<input type="text" class="form-control" id="firstname" placeholder="John" name="firstname" value="<?php echo showField("firstname");?>">
			</div>
			<div class="form-group">
				<label for="lastname">Lastname</label>
				<input type="text" class="form-control" id="lastname" placeholder="Doe" name="lastname" value="<?php echo showField("lastname");?>">
			</div>
			<div class="form-group">
				<label for="birthdate">Birthdate</label>
				<input type="text" class="form-control" id="birthdate" placeholder="DD.MM.YYYY" name="birthdate" value="<?php echo showField("birthdate");?>">
			</div>
		</div>
		<div class="col-md-6">
			<h2>2. Contact Information</h2>
			<div class="form-group">
				<label for="address">Address</label>
				<input type="text" class="form-control" id="address" placeholder="" name="address" value="<?php echo showField("address");?>">
			</div>
			<div class="form-group">
				<label for="zip">Zip</label>
				<input type="text" class="form-control" id="zip" placeholder="" name="zip" value="<?php echo showField("zip");?>">
			</div>
			<div class="form-group">
				<label for="city">City</label>
				<input type="text" class="form-control" id="city" placeholder="" name="city" value="<?php echo showField("city");?>">
			</div>
			<div class="form-group">
				<label for="phone">Phone</label>
				<input type="text" class="form-control" id="phone" placeholder="" name="phone" value="<?php echo showField("phone");?>">
			</div>
		</div>
		<div class="col-md-12">
			<h2>3. Login Information</h2>
			<div class="form-group">
				<label for="emailaddress">Email Address</label>
				<input type="text" class="form-control" id="emailaddress" placeholder="john.doe@domain.com" name="emailaddress" value="<?php echo showField("emailaddress");?>">
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="password">Password</label>
				<input type="password" class="form-control" id="password" placeholder="" name="password">
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="password2">Confirm Password</label>
				<input type="password" class="form-control" id="password2" placeholder="" name="password2">
			</div>
		</div>
		<div class="pull-right">
			<button type="submit" class="btn btn-default">Create Account</button>
		</div>
	</div>
</form>
<?php
	if (array_key_exists("values", $_SESSION)){
		unset($_SESSION["values"]);
	}
?>