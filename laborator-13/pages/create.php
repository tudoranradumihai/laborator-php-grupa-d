<?php
if ($_SERVER["REQUEST_METHOD"]=="POST"){
	// se face conexiunea la baza de date
	$connection = mysqli_connect("localhost","root","","gdl14");
	// se stabileste variabila $validation ca fiind flagul de verificare a validitatii formularului
	$validation = true;
	$validationEmail = true;

	$errors = array();
	$values = array();

	// se verifica campurile obligatorii
	$requiredFields = array("firstname","lastname","emailaddress","password");
	foreach ($requiredFields as $field){
		if (trim($_POST[$field])==""){
			$validation = false;
			array_push($errors,"Field '$field' is empty.");
		}
	}

	// in cazul in care adresa de email a fost completata
	if (trim($_POST["emailaddress"])!=""){
		// se verifica daca adresa este introdusa cu format valid
		if (!filter_var($_POST["emailaddress"],FILTER_VALIDATE_EMAIL)){
			$validation = false;
			$validationEmail = false;
			array_push($errors,"The email inserted is unvalid.");

		} else {
			// se verifica daca adresa exista deja in baza de date
			$query = "SELECT * FROM users WHERE emailaddress LIKE '$_POST[emailaddress]'";
			$result = mysqli_query($connection,$query);
			// daca rezultatul operatiei la baza de date are cel putin un rand, exista deja adresa respectiva in db
			if (mysqli_num_rows($result)!=0){
				$validation = false;
				$validationEmail = false;
				array_push($errors,"The email '$_POST[emailaddress]' already exists in the database.");
			}
		}
	}

	// in cazul in care a fost introdusa prima parola
	if (trim($_POST["password"])!=""){
		// se verifica daca nu a fost introdusa si a doua parola
		if (trim($_POST["password2"])==""){
			$validation = false;
			array_push($errors,"You need to confirm your password.");
		} else if (strlen($_POST["password"])<8){
			// se verifica daca parola nu are minim 8 caractere
			$validation = false;
			array_push($errors,"You password must have at least 8 characters.");
		} else if ($_POST["password"]!=$_POST["password2"]){
			// se verifica daca parolele nu coincid
			$validation = false;
			array_push($errors,"Your passwords don't match.");
		}
	}

	// se parcurg toate campurile exceptand parolele pentru a le salva intr-un array in cazul in care au fost completate deja; acest array va fi transmis in sesiune pentru a fi utilizat la recompletarea formularului
	$savedFields = array("firstname","lastname","birthdate","address","zip","city","phone");
	if ($validationEmail){
		array_push($savedFields,"emailaddress");
	}
	foreach ($savedFields as $field){
		if (trim($_POST[$field])!=""){
			$values[$field] = $_POST[$field];
		}
	}

	// in functie de statusul validarii
	if ($validation){
		// se face variabila query ce va contine metoda de introducere a utilizatorului in baza de date
		$query = "INSERT INTO users (firstname,lastname,birthdate,address,zip,city,phone,emailaddress,password) VALUES ('$_POST[firstname]','$_POST[lastname]','$_POST[birthdate]','$_POST[address]','$_POST[zip]','$_POST[city]','$_POST[phone]','$_POST[emailaddress]','".md5($_POST["password"])."');";
		$result = mysqli_query($connection,$query);
		if($result){
			// daca inserarea a fost facuta cu success atunci se va trimite un email de confirmare
			echo "DA";
			$to = $_POST["emailaddress"];
			$subject = "Domain.com Account Confirmation";
			$message = "Dear $_POST[firstname] $_POST[lastname],\n\nBest Regards.";
			$headers = 'From: no-reply@magazinulcujucarii.com' . "\r\n" .
		    'Reply-To: contact@magazinulcujucarii.com' . "\r\n" .
		    'X-Mailer: PHP/' . phpversion();

			mail($to,$subject,$message,$headers);
		} else {
			echo "NU";
		}
	} else {
		// daca validarea este falsa, se vor trimite erorile si campurile completate inapoi la  formular
		$_SESSION["errors"] = $errors;
		$_SESSION["values"] = $values;
	}
	mysqli_close($connection);
	if(!$validation){
		header("Location: index.php?page=new");		
	}
} else {
	header("Location: index.php?page=new");
}