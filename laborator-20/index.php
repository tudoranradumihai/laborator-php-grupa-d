<?php
	$connection = mysqli_connect("localhost","root","","laborator-20-D");
	if($_SERVER["REQUEST_METHOD"]=="POST"){
		if ($_POST["question"]!=""){
		$query="INSERT INTO answers (question,answer,grade,user) VALUES ('$_POST[question]','$_POST[answer]',$_POST[grade],$_POST[user])";
		mysqli_query($connection,$query);
	}
	}
	$query = "SELECT * FROM users";
	$result = mysqli_query($connection,$query);
	$users = array();
	while($line = mysqli_fetch_assoc($result)){
		$users[$line["id"]] = $line;
	}
	$numberOfUsers = count($users);
	$user = $users[rand(1,$numberOfUsers)];
?>
<!DOCTYPE html>
<html>
	<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<h1>Test</h1>
			<form method="POST">
				<input type="hidden" name="user" value="<?php echo $user["id"]; ?>" />
				<div class="form-group">
				    <h1>Student: <?php echo $user["name"]; ?></h1>
				</div>
				<div class="form-group">
				    <label>Question:</label>
				    <textarea class="form-control" name="question"></textarea>
				</div>
				<div class="form-group">
				    <label>Answer:</label>
				    <textarea class="form-control" name="answer"></textarea>
				</div>
				<div class="form-group">
				    <label>Grade:</label>
				    <input type="text" class="form-control" name="grade" />
				</div>
				<button type="submit" class="btn btn-default">Submit</button>
			</form>

<?php
	
	$query = "SELECT * FROM answers";
	$result = mysqli_query($connection,$query);
	$results = array();
	while($line = mysqli_fetch_assoc($result)){
		if(array_key_exists(intval($line["user"]), $results)){
			$results[intval($line["user"])]["answers"]++;
			$results[intval($line["user"])]["points"] += intval($line["grade"]);
		} else {
			$results[intval($line["user"])] = array(
				"answers" => 1,
				"points" => intval($line["grade"])
			);
		}
	}
	echo '<table class="table">';
	foreach ($results as $key => $value){
		$grade = $value["points"]/$value["answers"];
		if ($grade>5){
			$class = "success";
		} else if (intval($grade)==5 && intval($grade)==$grade){
			$class = "warning";
		} else {
			$class = "danger";
		}
		echo "<tr class=\"$class\">";
		echo "<td>".$users[$key]["name"]."</td>";
		echo "<td>".$value["answers"]."</td>";
		echo "<td>".number_format($grade,2)."</td>";
		echo "</tr>";
	}
	echo '</table>';
	mysqli_close($connection);
?>
		</div>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
</html>