<?php
include "arrays.php";

$size = array(
	"small" => array(
		"w" => 200,
		"h" => 100
	),
	"big" => array(
		"w" => 350,
		"h" => 200
	)
);

if (array_key_exists("size", $_GET)){
	$dimension = $_GET["size"];
} else {
	$dimension ="big";
}

if (array_key_exists("watch", $_GET)){
	$key = $_GET["watch"];
	$name = $array[$key]["name"];
	$link = $array[$key]["link"];
	echo "<h1>".$name."</h1>";
	echo '<iframe width="'.
	$size[$dimension]["w"].'" height="'.
	$size[$dimension]["h"].'"
src="https://www.youtube.com/embed/'.$link.'">
</iframe>';
}
?>
<a href="index.php">Back</a>