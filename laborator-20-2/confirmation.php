<?php
session_start();

if($_SERVER["REQUEST_METHOD"]=="POST"){
	$errors = array();
	$validation = true;
	$required = array("fn","ln","em","p1","p2");
	foreach($required as $field){
		if (trim($_POST[$field])==""){
			$validation = false;
			$errors[] = "n-ai pus nimic in $field";
		}
	}
	if ($_POST["p1"]!=$_POST["p2"]){
		$validation = false;
		$errors[] = "nu is bune parolele";
	}
	$fields = array();
	$saved = array("fn","ln","em","p1");
	foreach($saved as $field){
		if ($_POST[$field]!=""){
			$fields[$field] = $_POST[$field];	
		}
	}
	if($validation){
		$content = json_encode($fields);
		if(!file_exists("users")){
			mkdir("users");
		}
		$file = fopen("users/$fields[fn].$fields[ln].txt","w");
		fputs($file,$content);
		fclose($file);
		header("location: list.php");	
	} else {
		$_SESSION["errors"] = $errors;
		$_SESSION["fields"] = $fields;
		header("location: index.php");
	}
} else {
	header("location: index.php");
}