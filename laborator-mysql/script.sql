/* creare baza de date */
CREATE DATABASE gdl11;
/* stergere baza de date */
DROP DATABASE gdl11;
/* creare tabela */
CREATE TABLE users (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	firstname VARCHAR(50) NOT NULL,
	lastname VARCHAR(50) NOT NULL,
	email VARCHAR(120) DEFAULT 'no-reply@domain.com',
	phone VARCHAR(16) 
);

/* stergere tabela */
DROP TABLE users;

/* inserare in tabela */
INSERT INTO users (firstname,lastname,email,phone)
	VALUES ("Radu","Tudoran","email","8989");

INSERT INTO users (firstname,lastname,email,phone)
	VALUES ("Radu","Tudoran","email","8989"),
	("Radu","Tudoran","email","8989");
