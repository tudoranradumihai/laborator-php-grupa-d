<?php
if (isset($error)){
	echo '<div class="alert alert-danger" role="alert">'.$error.'</div>';
}
/*
if (isset($emailaddress)){
	echo $emailaddress;
}
echo (isset($emailaddress)?$emailaddress:"");
*/
?>
<form id="users-login" method="POST" action="<?php URL::show("Users","connect") ?>">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="panel panel-default">
				<div class="panel-body">
					<h1>Login</h1>
					<div class="form-group">
						<label for="emailaddress">Email address *:</label>
						<input type="text" class="form-control" id="emailaddress" name="emailaddress" placeholder="" value="<?php echo (isset($emailaddress)?$emailaddress:""); ?>">
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" class="form-control" id="password" name="password" placeholder="" >
					</div>
					<div class="pull-right">
						<button type="submit" class="btn btn-default">Login</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>