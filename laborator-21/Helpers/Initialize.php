<?php

function initialize(){

	if(array_key_exists("C", $_GET)){
		$controller = $_GET["C"]."Controller";
	} else {
		$controller = "DefaultController";
	}
	if(array_key_exists("A", $_GET)){
		$action = $_GET["A"]."Action";
	} else {
		$action = "defaultAction";
	}
	if(class_exists($controller)){
		$object = new $controller();
		if(method_exists($object, $action)){
			$object->$action();
		} else {
			echo "ERROR: Method '$controller::$action' does not exist."."<br />";
		}
	} else {
		echo "ERROR: Class '$controller' does not exist."."<br />";
	}


}