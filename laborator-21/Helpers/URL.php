<?php

Class URL {

	public static function create($controller, $action, $identifier = NULL){
		$url = "index.php?C=$controller&A=$action";
		if ($identifier!=NULL){
			$url .= "&ID=".intval($identifier);
		}
		return $url;
	}

	public static function show($controller,$action,$identifier = NULL){
		echo self::create($controller,$action,$identifier);
	}

	public static function redirect($controller,$action,$identifier = NULL){
		header("Location: ".self::create($controller,$action,$identifier));
	}

}


