<?php

function __autoload($class){
	$folders = array(
		"Controllers","Helpers","Models"
	);
	foreach($folders as $folder){
		$filename = "$folder/$class.php";
		if(file_exists($filename)){
			include $filename;
		}
	}
}