<?php

Class Database {

	private $connection;

	public function __construct(){
		$configuration = require "configuration.php";
		$this->connection = new mysqli(
			$configuration["database"]["hostname"],
			$configuration["database"]["username"],
			$configuration["database"]["password"],
			$configuration["database"]["database"]
		);
	}

	public function query($query){
		return $this->connection->query($query);
	}

	public function __destruct(){
		$this->connection->close();
	}

}