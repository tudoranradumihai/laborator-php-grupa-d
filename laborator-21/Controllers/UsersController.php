<?php

Class UsersController extends Controller {

	public function defaultAction(){
	
	}

	public function newAction(){
		include "Views/Users/new.php";
	}

	public function createAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			// de facut validari in php pentru create
			$user = new Users(NULL,$_POST["firstname"],$_POST["lastname"],$_POST["emailaddress"],md5($_POST["password"]));
			$result = Users::insert($user);
			if ($result){
				echo "OK";
			}
		} else {	
			URL::redirect("Users","new");
		}
	}

	public function loginAction(){
		if (array_key_exists("users-connect-value", $_SESSION)){
			$emailaddress = $_SESSION["users-connect-value"];
			unset($_SESSION["users-connect-value"]);
		}
		if (array_key_exists("users-connect-error", $_SESSION)){
			$error = $_SESSION["users-connect-error"];
			unset($_SESSION["users-connect-error"]);
		}
		include "Views/Users/login.php";
	}

	public function connectAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$result = Users::checkCredentials($_POST["emailaddress"],md5($_POST["password"]));
			if ($result){
				setcookie("user",$result["id"],time()+3600);
				unset($result["password"]);
				$_SESSION["user"] = $result;
				URL::redirect("Users","welcome");
			} else {
				$result = Users::checkEmailAddress($_POST["emailaddress"]);
				if ($result){
					$error = "Password for '$_POST[emailaddress]' is not valid.";
					$_SESSION["users-connect-value"] = $_POST["emailaddress"];
				} else {
					$error = "There is no account with the following email '$_POST[emailaddress]'.";
				}
				$_SESSION["users-connect-error"] = $error;
				URL::redirect("Users","login");
			}
		} else {
			URL::redirect("Users","login");
		}
	}

	public function disconnectAction(){
		setcookie("user",NULL,time()-3600);
		if(array_key_exists("user", $_SESSION)){
			unset($_SESSION["user"]);
		}
		URL::redirect("Default","default");
	}

	public static function loginStatus(){
		if(array_key_exists("user", $_COOKIE)){
			$user = $_SESSION["user"];
		}
		include "Views/Users/loginStatus.php";
	}

	public function welcomeAction(){
		if(array_key_exists("user", $_COOKIE)){
			$user = $_SESSION["user"];
			include "Views/Users/welcome.php";
		} else {
			URL::redirect("Users","login");
		}
	}
}
