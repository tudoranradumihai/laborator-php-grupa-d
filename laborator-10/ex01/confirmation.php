<?php

// definire constanta domeniu pe care ne aflam
define("DOMAIN","http://localhost/");

// se verifica daca HTTP requestul este facut cu metoda POST
if($_SERVER["REQUEST_METHOD"]=="POST"){
	// TEMA: de implementat cu http://php.net/manual/ro/function.preg-match.php verificarea elementului $_SERVER HTTP_REFERER daca incepe cu string-ul introdus in constanta DOMAIN
	
	// se verifica daca stringul $_SERVER HTTP_REFERER incepe cu tring-ul introdus in constanta DOMAIN - http://php.net/manual/en/function.strpos.php - strpos returneaza falsa daca nu gaseste substringul in string si returneaza pozitia in format int a primei litere a substringului gasit in string.
	if(strpos($_SERVER["HTTP_REFERER"],DOMAIN) === 0){
		var_dump($_POST);
	} else {
		header("Location: ".$_SERVER["HTTP_REFERER"]);
	}
} else {
	header("Location: http://localhost/laborator-php-grupa-d/laborator-10/ex01/");
}