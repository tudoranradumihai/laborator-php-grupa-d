<?php

Class UsersController extends Controller {

	public function defaultAction(){
	
	}

	public function newAction(){
		if(array_key_exists("users-create-errors", $_SESSION)){
			$errors = $_SESSION["users-create-errors"];
			unset($_SESSION["users-create-errors"]);
		}
		if(array_key_exists("users-create-values", $_SESSION)){
			$values = $_SESSION["users-create-values"];
			unset($_SESSION["users-create-values"]);
		} else {
			$values = array();
		}
		include "Views/Users/new.php";
	}

	public function createAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$validation = true;
			$errors = array();
			$required = array("firstname","lastname","emailaddress","password");
			foreach($required as $field){
				if(trim($_POST[$field])==""){
					$validation = false;
					array_push($errors,"Field '$field' is required.");
				}
			}			
			if($validation){
				if($_POST["password2"]==""){
					$validation = false;
					array_push($errors,"You have not confirmed your password.");
				} else if($_POST["password"]!=$_POST["password2"]){
					$validation = false;
					array_push($errors,"Passwords don't match.");
				}
			}
			if($validation){
				$user = new Users(NULL,$_POST["firstname"],$_POST["lastname"],$_POST["emailaddress"],md5($_POST["password"]));
				$result = Users::insert($user);
				if ($result){
					echo "OK";
				}
			} else {
				$values = array();
				$fields = array("firstname","lastname","emailaddress");
				foreach($fields as $field){
					if($_POST[$field]!=""){
						$values[$field] = $_POST[$field];
					}
				}
				$_SESSION["users-create-errors"] = $errors;
				$_SESSION["users-create-values"] = $values;
				URL::redirect("Users","new");
			}
		} else {	
			URL::redirect("Users","new");
		}
	}

	public function loginAction(){
		if (array_key_exists("users-connect-value", $_SESSION)){
			$emailaddress = $_SESSION["users-connect-value"];
			unset($_SESSION["users-connect-value"]);
		}
		if (array_key_exists("users-connect-error", $_SESSION)){
			$error = $_SESSION["users-connect-error"];
			unset($_SESSION["users-connect-error"]);
		}
		include "Views/Users/login.php";
	}

	public function connectAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$user = Users::checkCredentials($_POST["emailaddress"],md5($_POST["password"]));
			if ($user){
				setcookie("user",$user["id"],time()+3600);
				unset($user["password"]);
				$_SESSION["user"] = $user;
				URL::redirect("Users","welcome");
			} else {
				$result = Users::checkEmailAddress($_POST["emailaddress"]);
				if ($result){
					$error = "Password for '$_POST[emailaddress]' is not valid.";
					$_SESSION["users-connect-value"] = $_POST["emailaddress"];
				} else {
					$error = "There is no account with the following email '$_POST[emailaddress]'.";
				}
				$_SESSION["users-connect-error"] = $error;
				URL::redirect("Users","login");
			}
		} else {
			URL::redirect("Users","login");
		}
	}

	public function disconnectAction(){
		setcookie("user",NULL,time()-3600);
		if(array_key_exists("user", $_SESSION)){
			unset($_SESSION["user"]);
		}
		URL::redirect("Default","default");
	}

	public static function loginStatus(){
		if(array_key_exists("user", $_COOKIE)){
			$user = $_SESSION["user"];
		}
		include "Views/Users/loginStatus.php";
	}

	public function welcomeAction(){
		if(array_key_exists("user", $_COOKIE)){
			$user = $_SESSION["user"];
			include "Views/Users/welcome.php";
		} else {
			URL::redirect("Users","login");
		}
	}

	public function editAction(){
		if(array_key_exists("user", $_COOKIE)){
			$user = $_SESSION["user"];
			include "Views/Users/edit.php";
		} else {
			URL::redirect("Users","login");
		}
	}

	public function updateAction(){
		if(array_key_exists("user", $_COOKIE)){
			if($_SERVER["REQUEST_METHOD"]=="POST"){
				$user = new Users($_POST["id"],$_POST["firstname"],$_POST["lastname"],$_POST["emailaddress"],NULL);
				$result = Users::update($user);
				if ($result){
					$user = Users::findByID($_COOKIE["user"]);
					unset($user["password"]);
					$_SESSION["user"] = $user;
				}
				URL::redirect("Users","edit");
			}
		} else {
			URL::redirect("Users","login");
		}
	}

	public function updatePasswordAction(){
		if(array_key_exists("user", $_COOKIE)){
			if($_SERVER["REQUEST_METHOD"]=="POST"){
				$user = new Users($_POST["id"],NULL,NULL,NULL,md5($_POST["password"]));
				$result = Users::update($user);
				if ($result){
					// send ok message
				}
				URL::redirect("Users","edit");
			}
		} else {
			URL::redirect("Users","login");
		}
	}
}
