$(document).ready(function(){
	/*
	$("#users-new").validate({
		rules: {
			firstname: "required",
			lastname: "required",
			emailaddress: {
				required: true,
				email: true
			},
			password: {
				required: true
			},
			password2: {
				required: true,
				equalTo:  "#password1"
			}
		},
		submitHandler: function(form) {
			form.submit();
	    }
	});
*/
	$("#users-edit").validate({
		rules: {
			firstname: "required",
			lastname: "required",
			emailaddress: {
				required: true,
				email: true
			}
		},
		submitHandler: function(form) {
			form.submit();
	    }
	});

	$("#users-edit-password").validate({
		rules: {
			oldpassword: {
				required: true
			},
			password: {
				required: true
			},
			password2: {
				required: true,
				equalTo:  "#password1"
			}
		},
		submitHandler: function(form) {
			form.submit();
	    }
	});

	$("#users-login").validate({
		rules: {
			emailaddress: {
				required: true,
				email: true
			},
			password: {
				required: true
			}
		},
		submitHandler: function(form) {
			form.submit();
	    }
	});
});