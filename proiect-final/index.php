<?php
session_start();
include "Helpers/Initialize.php";
include "Helpers/Autoload.php";
?>
<!DOCTYPE html>
<html>
	<head>
		<?php include 'Resources/Partials/HeaderResources.php'; ?>
	</head>
	<body>
		<div class="container">
			<?php include 'Resources/Partials/Header.php' ?>
			<?php initialize(); ?>
			<?php include 'Resources/Partials/Footer.php' ?>
		</div>
		<?php include 'Resources/Partials/FooterResources.php'; ?>
	</body>
</html>






