<?php

Class Users {

	public $id;
	public $firstname;
	public $lastname;
	public $emailaddress;
	public $password;
	public $createddate;
	public $updateddate;

	public function __construct($id,$firstname,$lastname,$emailaddress,$password,$createddate = NULL, $updateddate = NULL){
		$this->id 			= $id;
		$this->firstname 	= $firstname;
		$this->lastname 	= $lastname;
		$this->emailaddress = $emailaddress;
		$this->password 	= $password;
		$this->createddate 	= $createddate;
		$this->updateddate 	= $updateddate;
	}

	public static function insert($object){
		$table = strtolower(get_class($object));
		$properties = array();
		$values = array();
		foreach ($object as $property => $value){
			if ($value!=NULL){
				array_push($properties,$property);
				array_push($values, '"'.$value.'"');
			}
		}
		$query = "INSERT INTO $table (".implode(",",$properties).") VALUES (".implode(",",$values).");";
		$database = new Database();
		return $database->query($query);
	}

	public static function checkCredentials($emailaddress,$password){
		$query = "SELECT * FROM users WHERE emailaddress LIKE '$emailaddress' AND password LIKE '$password';";
		$database = new Database();
		$result = $database->query($query);
		if ($result->num_rows!=0){
			return $result->fetch_assoc();
		} else {
			return NULL;
		}
	}

	public static function checkEmailAddress($emailaddress){
		$query = "SELECT * FROM users WHERE emailaddress LIKE '$emailaddress';";
		$database = new Database();
		$result = $database->query($query);
		if ($result->num_rows!=0){
			return $result->fetch_assoc();
		} else {
			return NULL;
		}
	}

	public static function update($object){
		$table = strtolower(get_class($object));
		$properties = array();
		foreach($object as $property => $value){
			if ($property!="id" && $value!=NULL){
				array_push($properties,"$property = '$value'");
			}
		}
		$query = "UPDATE $table SET ".implode(", ",$properties)." WHERE id=$object->id;";
		$database = new Database();
		return $database->query($query);
	}

	public static function findByID($id){
		$query = "SELECT * FROM users WHERE id=$id;";
		$database = new Database();
		$result = $database->query($query);
		if ($result->num_rows!=0){
			return $result->fetch_assoc();
		} else {
			return NULL;
		}
	}

}