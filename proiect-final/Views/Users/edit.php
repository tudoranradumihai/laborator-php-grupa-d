<h1>Edit Profile</h1>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin suscipit fringilla dui, in viverra ex pharetra id. Aliquam erat volutpat. Sed pharetra, lorem eget vestibulum maximus, nibh quam tincidunt enim, quis blandit risus nisl in elit. Fusce eget malesuada mi, sed auctor felis. Nam posuere accumsan ante quis ornare. Integer.</p>
<form id="users-edit" method="POST" action="<?php URL::show("Users","update"); ?>">
	<input type="hidden" name="id" value="<?php echo $user["id"]; ?>" >
	<div class="row">
		<div class="col-md-12">
			<h2>1. Personal Information</h2>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="firstname">Firstname *:</label>
				<input type="text" class="form-control" id="firstname" name="firstname" placeholder="John" value="<?php echo $user["firstname"] ?>">
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="lastname">Lastname *:</label>
				<input type="text" class="form-control" id="lastname" name="lastname" placeholder="Doe" value="<?php echo $user["lastname"] ?>">
			</div>
		</div>
		<div class="col-md-12">
			<h2>2. Contact Information</h2>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label for="emailaddress">Email address *:</label>
				<input type="text" class="form-control" id="emailaddress" name="emailaddress" placeholder="john.doe@domain.com" disabled value="<?php echo $user["emailaddress"] ?>">
			</div>
		</div>
		<div class="col-md-12">
			<div class="pull-right">
				<button type="submit" class="btn btn-primary">Edit Profile</button>
			</div>
		</div>
	</div>
</form>
<form id="users-edit-password" method="POST" action="<?php URL::show("Users","updatePassword"); ?>">
	<input type="hidden" name="id" value="<?php echo $user["id"]; ?>" >
	<div class="row">
		<div class="col-md-12">
			<h1>Edit Password</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin suscipit fringilla dui, in viverra ex pharetra id. Aliquam erat volutpat. Sed pharetra, lorem eget vestibulum maximus, nibh quam tincidunt enim, quis blandit risus nisl in elit. Fusce eget malesuada mi, sed auctor felis. Nam posuere accumsan ante quis ornare. Integer.</p>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="oldpassword">Old password *:</label>
				<input type="password" class="form-control" id="oldpassword" name="oldpassword">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="password1">Password *:</label>
				<input type="password" class="form-control" id="password1" name="password">
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="password2">Confirm password *:</label>
				<input type="password" class="form-control" id="password2" name="password2">
			</div>
		</div>
		<div class="col-md-12">
			<div class="pull-right">
				<button type="submit" class="btn btn-primary">Edit Password</button>
			</div>
		</div>
	</div>
</form>